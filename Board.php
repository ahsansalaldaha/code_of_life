<?php

/**
 * Board
 */
class Board {

	private $martix;
	private $rows;
	private $cols;

	function __construct($cols, $rows) {

		if (!$rows && !$cols) {

			throw new \Exception('Cant have board with no row or columns', 100);
		}

		$this->rows = $rows;
		$this->cols = $cols;
		$this->martix = [];

		for ($i = 0; $i < $this->rows; $i++) {
			$this->martix[$i] = [];
			for ($j = 0; $j < $this->cols; $j++) {
				$this->martix[$i][$j] = new Cell(false);
			}
		}

	}

	public function print() {

		for ($i = 0; $i < $this->rows; $i++) {
			echo "\n";

			// Separator
			for ($k = 0; $k < ($this->cols * 4) + 1; $k++) {
				echo "-";
			}
			echo "\n";

			for ($j = 0; $j < $this->cols; $j++) {
				echo "|";

				echo " ";
				if (($this->martix[$i][$j])->isPopulated()) {
					echo "1";
				} else {
					echo "0";
				}
				echo " ";
			}
			echo "|";
		}

		echo "\n";
		// Last Separator
		// Separator
		for ($k = 0; $k < ($this->cols * 4) + 1; $k++) {
			echo "-";
		}
		echo "\n";

	}

	public function isValidCell($x, $y) {
		if ($x >= $this->rows) {
			return false;
		} else if ($y >= $this->cols) {
			return false;
		} else if ($x < 0) {
			return false;
		} else if ($y < 0) {
			return false;
		}

		return true;

	}

	public function getCellAt($x, $y) {
		if ($this->isValidCell($x, $y)) {
			return $this->martix[$x][$y];
		} else {

			throw new \Exception("Out of range", 102);
		}
		return $this;

	}

	public function setCellAt($x, $y, $cell) {
		if ($this->isValidCell($x, $y)) {
			$this->martix[$x][$y] = $cell;
		} else {

			throw new \Exception("Out of range", 102);
		}
		return $this;
	}

	public function getNeighbours($x, $y) {

		$neighbours = [];
		for ($i = -1; $i <= 1; $i++) {
			for ($j = -1; $j <= 1; $j++) {
				if ($i == 0 && $j == 0) {
					continue;
				}
				try {

					$dim_x = $x + $i;
					$dim_y = $y + $j;

					$neighbours[] = $this->getCellAt($dim_x, $dim_y);

				} catch (\Exception $e) {

					if ($e->getCode() == 102) {
						// for this expection the behaviour is to ignore
					} else {
						// Handle like wise rethrough or what
					}
				}

			}

		}

		return $neighbours;

	}

	public function randInitialize() {
		for ($i = 0; $i < $this->rows; $i++) {
			for ($j = 0; $j < $this->cols; $j++) {
				$this->martix[$i][$j] = new Cell(rand(0, 1));
			}
		}

	}

	public function initialize() {
		$val = true;
		for ($i = 0; $i < $this->rows; $i++) {
			for ($j = 0; $j < $this->cols; $j++) {
				$this->martix[$i][$j] = new Cell($val);
			}
		}

	}

}