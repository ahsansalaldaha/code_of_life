<?php

// You can implement more rules by implementing this interface

interface Rule {
	public function apply($cell, $neighbours, $populated_neighbours);
}