<?php

/**
 * UnderPopulationRule
 */

class UnderPopulationRule implements Rule {

	public function apply($cell, $neighbours, $populated_neighbours) {

		if ($cell->isPopulated()) {
			if ($populated_neighbours < 2) {
				return $cell->unPopulate();
			}
		}
		return $cell;
	}
}