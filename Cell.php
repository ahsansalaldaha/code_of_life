<?php

/**
 * Cell
 */
class Cell {

	private $value;

	function __construct($value) {
		$this->value = $value;
	}

	public function isPopulated() {
		return $this->value == true;
	}

	public function populate() {
		$this->value = true;
		return $this;
	}

	public function unPopulate() {
		$this->value = false;
		return $this;
	}

	public function set($value) {
		$this->value = $value;
		return $this;
	}
	public function get() {
		return $this->value;
	}

}