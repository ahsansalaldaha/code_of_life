<?php

/**
 * OverPopulationRule
 */

class OverPopulationRule implements Rule {

	public function apply($cell, $neighbours, $populated_neighbours) {

		if ($cell->isPopulated()) {
			if ($populated_neighbours > 3) {
				return $cell->unPopulate();
			}
		}
		return $cell;
	}
}