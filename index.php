<?php

spl_autoload_register(function ($class) {
	include (__DIR__ . "/" . $class . ".php");
});

$game_of_life = new GameOfLife();
$game_of_life->run();
