<?php

/**
 * Game Of Life
 */
class GameOfLife {

	private $board;
	private $rules = [];
	private $rows = 3;
	private $cols = 3;

	function __construct() {
		$this->board = new Board($this->rows, $this->cols);
		$this->rules[] = new UnderPopulationRule();
		$this->rules[] = new OverPopulationRule();
		$this->rules[] = new ReproductionRule();
	}

	public function run() {
		$this->board->initialize();
		// $this->board->randInitialize();

		while (1) {
			$this->board->print();
			$this->tick();

			// if we like we can check here if all die then stop
			sleep(2);
		}

	}

	public function tick() {

		for ($i = 0; $i < $this->rows; $i++) {
			for ($j = 0; $j < $this->cols; $j++) {

				$cell = $this->board->getCellAt($i, $j);

				$neighbours = $this->board->getNeighbours($i, $j);

				$neighbours_count = count($neighbours);

				$populated = $this->findPopulatedNeighbours($neighbours);

				foreach ($this->rules as $key => $rule) {

					$cell = $rule->apply($cell, $neighbours, $populated);
					$this->board->setCellAt($i, $j, $cell);
				}

			}
		}

	}

	// pass by reference for optimization
	public function findPopulatedNeighbours(&$neighbours) {
		$populated = 0;
		foreach ($neighbours as $neighbour) {
			if ($neighbour->isPopulated()) {
				$populated++;
			}
		}
		return $populated;
	}

}
